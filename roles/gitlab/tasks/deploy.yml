---
- name: add helm repos
  community.kubernetes.helm_repository:
    name: "{{ item.name }}"
    repo_url: "{{ item.url }}"
  loop:
  - name: bitnami
    url: https://charts.bitnami.com/bitnami
  - name: gitlab
    url: https://charts.gitlab.io

- name: install redis
  community.kubernetes.helm:
    name: gitlab-redis
    chart_ref: bitnami/redis
    chart_version: "{{ redis_chart_version }}"
    release_namespace: "{{ ansible_operator_meta.namespace }}"
    values: "{{ lookup('template', 'redis-values.yml.j2') | from_yaml }}"

- import_tasks: gcp.yml
  when:
  - provider == "gcp"

- import_tasks: azure.yml
  when:
  - provider == "azure"

- import_tasks: admission_controllers.yml

- name: create oidc client
  community.kubernetes.k8s:
    definition:
      apiVersion: keycloak.zeero.io/v1
      kind: KeycloakClient
      metadata:
        name: gitlab
        namespace: "{{ ansible_operator_meta.namespace }}"
      spec:
        domain: "{{ domain }}"
        client_id: gitlab
        redirect_uri: "https://gitlab.{{ domain }}/users/auth/openid_connect/callback"
        
- name: get oidc client secret
  community.kubernetes.k8s_info:
    kind: Secret
    name: oidc-client
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: oidc_client_secret_info
  delay: 3
  retries: 40
  until: "oidc_client_secret_info.resources"

- set_fact:
    oidc_client_secret: "{{ oidc_client_secret_info.resources[0].data.client_secret | b64decode }}"

- name: create keycloak provider
  community.kubernetes.k8s:
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: keycloak-provider
        namespace: "{{ ansible_operator_meta.namespace }}"
      stringData:
        provider: |
          name: openid_connect
          label: Keycloak
          icon: https://i.imgur.com/1KV4FZb.png
          args:
            issuer: "https://keycloak.{{ domain }}/auth/realms/master"
            discovery: true
            client_options:
              identifier: "gitlab"
              secret: "{{ oidc_client_secret }}"
              redirect_uri: "https://gitlab.{{ domain }}/users/auth/openid_connect/callback"

- name: get smtp username
  community.kubernetes.k8s_info:
    kind: Secret
    name: smtp-credentials
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: smtp_credentials_secret

- set_fact:
    smtp_username: "{{ smtp_credentials_secret.resources[0].data.username | b64decode }}"

- name: install gitlab
  community.kubernetes.helm:
    name: gitlab
    chart_ref: gitlab/gitlab
    chart_version: "{{ gitlab_chart_version }}"
    release_namespace: "{{ ansible_operator_meta.namespace }}"
    values: "{{ lookup('template', 'gitlab-values.yml.j2') | from_yaml }}"
  register: gitlab_installed

- name: wait until gitlab webservice is ready
  community.kubernetes.k8s_info:
    name: gitlab-webservice-default
    api_version: apps/v1
    kind: Deployment
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: gitlab_webservice
  delay: 3
  retries: 200
  until: "gitlab_webservice.resources[0].status.readyReplicas is defined"
